const express = require("express");
const router = express.Router();
const doneRoute = require("./done");
const path = require("path");
const { check, validationResult } = require("express-validator");
const UsersService = require(path.join(__dirname,"../UsersService"))
const TasksService = require(path.join(__dirname,"../TasksService"))
const jwt = require('jsonwebtoken');

const validation = [
  check('task')
    .trim()
    .isLength({ min: 3 })
    .escape()
    .withMessage('A name is required')
] 

const validationForUser = [
  check('email')
  .trim()
  .isEmail()
  .normalizeEmail()
  .withMessage('A valid email address is required'),
]


module.exports = () => {
  const SECRET_KEY = 'your_very_secret_key_here';

  router.post('/api/register', validationForUser ,async (req, res) => {
    try {

      const error = req.session ? req.session.errors : false;
      const successMessage = req.session ? req.session.message : false;
      req.session = {};

      await UsersService.addEntry(req.body.email, req.body.password, req.body.firstName, req.body.lastName);

      const users = await UsersService.getList();

      res.json(users); // Send the updated task object back to the client
    } catch (error) {
      console.error('Error marking task as done:', error);
      res.status(404).send(error.message); // Send a 404 Not Found error message
    }
  });

  router.post('/api/tasks/:taskName/done', async (req, res) => {
    try {
      const updatedTask = await TasksService.markDone(req.params.taskName);
      res.json(updatedTask); // Send the updated task object back to the client
    } catch (error) {
      console.error('Error marking task as done:', error);
      res.status(404).send(error.message); // Send a 404 Not Found error message
    }
  });

  router.post('/api/login', async (req, res)=>{
      const { email, password } = req.body;
      UsersService.loggingIn(email, password)
        .then((result) => {
          const token = jwt.sign({ id: result.userId }, SECRET_KEY, { expiresIn: '5m' });
          req.session.userId = result.userId;
          res.status(200).send({ token});
        }).catch((err) => {
          res.status(400);
        });
  });

  const authenticateToken = (req, res, next) => {
    const authHeader = req.headers['authorization'];
    console.log(authHeader);
    const token = authHeader && authHeader.split(' ')[1];
    if (!token) return res.sendStatus(401);
    jwt.verify(token, SECRET_KEY, (err, decoded) => {
      if (err) return res.sendStatus(403);
      next();
    });
  };
  // Adding the task
  router.post('/api/task', authenticateToken, async (req, res) => {
    // Assuming userID is stored in the JWT token
    try {
      const { title, description} = req.body;
      const userID = req.session.userId; 
      console.log(userID)
      const tasks = await TasksService.addEntry(userID, title, description);
      res.status(200).send(tasks);
    } catch (error) {
      res.status(500).send({ error: 'Failed to fetch tasks' });
    }
  });

  router.delete('/api/delete/:taskName', authenticateToken, async (req, res) => {
    // Assuming userID is stored in the JWT token
    try {
      const taskName = req.params.taskName;
      const userID = req.session.userId; 
      const response = await TasksService.deleteTask(userID, taskName);
      res.status(200).send(response)
    } catch (error) {
      res.status(400).send({ error: 'Failed to delete' });
    }
  });

  router.patch("/api/done/:taskName", authenticateToken, async(req, res) => {
      try {
        const taskName = req.params.taskName;
        const userID = req.session.userId; 
        console.log(userID)
        const response = await TasksService.markDone(userID, taskName);
        res.status(200).send(response)
      } catch (error) {
        res.status(400).send({ error: 'Failed to delete' });
      }
    }
  );

  router.patch("/api/:taskName/change", authenticateToken, async(req, res) => {
    try {
      const taskName = req.params.taskName;
      const taskDescription = req.body.taskDescription;
      const userID = req.session.userId; 
      console.log(userID)
      const response = await TasksService.changeTask(userID, taskName, taskDescription);
      res.status(200).send(response)
    } catch (error) {
      res.status(400).send({ error: 'Failed to delete' });
    }
  });

  router.get("/api/tasks", authenticateToken, async(req, res)=>{
    try{
      const userID = req.session.userId;
      const tasks = await TasksService.getList(userID);
      res.status(200).send(tasks)
    }catch(error){
      res.status(400).send({error:'Failed to download'})
    }
  });

  router.get("/api/doneTasks", authenticateToken, async(req, res)=>{
    try{
      const userID = req.session.userId;
      const tasks = await TasksService.getDoneList(userID);
      res.status(200).send(tasks)
    }catch(error){
      res.status(400).send({error:'Failed to download'})
    }
  })




  router.use("/done", doneRoute());

  return router;
};
