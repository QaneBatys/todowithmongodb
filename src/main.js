const express = require("express");
const makeStoppable = require("stoppable");
const http = require("http");
const path = require("path");
const cookieSession = require("cookie-session");
const bodyParser = require("body-parser");
const { error } = require("console");
const mongoose = require('mongoose');
const config = require(path.join(__dirname,"../src/config"));


async function connectToMongoose() {
  return mongoose.connect(config.mongodb.url);
}

const app = express();
const server = makeStoppable(http.createServer(app));

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.json());

app.use(
  cookieSession({
    name: "session",
    keys: ["Ghdur687399s7w", "hhjjdf89s866799"],
  })
);

app.set("view engine", "ejs");
app.set("views", path.join(__dirname, "./views"));

app.use(express.static(path.join(__dirname, "./static")));

const routes = require("./routes");

module.exports = () => {
  app.use("/", routes());

  const stopServer = () => {
    return new Promise((resolve) => {
      server.stop(resolve);
    });
  };

  return new Promise((resolve) => {
    connectToMongoose()
    .then(() => {
      console.info("Successfully connected to MongoDB");
      server.listen(3000);
    })
    .catch((error) => {
      console.error(error);
    });
  });
};
