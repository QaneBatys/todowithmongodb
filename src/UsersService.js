const path = require('path');
const UserModel =require(path.join(__dirname,"./models/mongoose/User.js"));
const { error } = require('protractor');
const bcrypt = require('bcrypt');

class UsersService {
    static async addEntry(email, password, firstName, lastName){
        const newUser = new UserModel({
            email:email,
            password:password,
            firstName:firstName,
            secondName:lastName
        })

        return newUser.save();
    }

    static async getList(){
        return UserModel.find({}).exec();
    }

    static async loggingIn( email, submittedPassword){
        try {
            // Find the user by email
            const user = await UserModel.findOne({ email: email.toLowerCase().trim() });
            if (user === null) {
              throw new Error("Error");
            };
      
            // Compare the submitted password with the user's stored hashed password
            const isMatch = await bcrypt.compare(submittedPassword, user.password);
            if (!isMatch) {
              throw new Error('doesnot work');
            }
            // Authentication successful
            return { success: true, userId: user._id, message: "Authentication successful" };
          } catch (error) {
            console.error('Error authenticating user:', error);
            return { success: false, message: "An error occurred during authentication" };
          }
    }
}

module.exports = UsersService;