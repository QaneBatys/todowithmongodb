const path = require('path');
const { error } = require('protractor');
const TaskModel = require(path.join(__dirname,'./models/mongoose/Task.js'));

class TasksService {
    static async getList(userID){
      return TaskModel.find({userID:userID, done: false}).exec();
    }

    static async getDoneList(userID){
      return TaskModel.find({userID:userID, done: true}).exec();
    }

    static async markDone(userID, taskName){
      const updated = TaskModel.findOneAndUpdate({userID:userID, taskName: taskName}, {done:true}, {new:true});
      if(!updated){
        return new error('loh');
      }

      return updated;
    }

    static async addEntry(userID, taskName, taskDescription){
      const added = await TaskModel.findOne({userID:userID, taskName:taskName, taskDescription: taskDescription}).exec();
      if(added){
        throw new error("The same task");
      }

      const task = new TaskModel({userID:userID, taskName:taskName, taskDescription:taskDescription});
      return task.save();
    }
    
    static async deleteTask(userID, taskName){
      const deletedTask =  TaskModel.findOneAndDelete({userID:userID, taskName: taskName});
      if(!deletedTask){
        return new error("Error");
      }

      return deletedTask;
    }

    static async changeTask(userID, taskName, taskDescription){
      const changed = TaskModel.findOneAndUpdate({userID:userID, taskName: taskName}, {taskDescription:taskDescription}, {new:true});
      if(!changed){
        return new error('loh');
      }

      return changed;
    }
}

module.exports = TasksService;