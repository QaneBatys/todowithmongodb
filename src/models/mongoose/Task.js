const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const TaskSchema = mongoose.Schema({
    userID: { type: Schema.Types.ObjectId, ref: 'User' },
    taskName: {
      type: String,
      required: true,
      index: { unique: true },
      trim: true,
    },
    taskDescription:{
        type: String,
        required: true,
    },
    done:{
        type:Boolean,
        default:false,
    }
  }, { timestamps: true });


  module.exports = mongoose.model("Task", TaskSchema);